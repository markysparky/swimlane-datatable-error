This repo is to demonstrate a problem running a production build containing ionic and swimlane/ngx-datatable

This works:
```bash
npm install
npm run ionic:serve
# Works! home page shows with data table as normal
```

To get the error:

```bash
npm install
npm run build --prod
cd www

# Serve the www folder (assumes you have done npm install -g serve)
serve 


# Serving!                                       │
#   │                                                  │
#   │   - Local:            http://localhost:5000

```

Visit localhost:5000 and you will see the error in the console

```
main.js:1 Uncaught TypeError: Cannot read property 'call' of undefined
    at e (vendor.js:1)
    at Object../src/datatable.module.ts (vendor.js:1)
    at e (vendor.js:1)
    at Object../src/index.ts (vendor.js:1)
    at e (vendor.js:1)
    at vendor.js:1
    at vendor.js:1
    at n (vendor.js:1)
    at Object.<anonymous> (vendor.js:1)
    at e (vendor.js:1)
```
